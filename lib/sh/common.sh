#!/usr/scripts/env bash

##############################################################################
###  Variables
##############################################################################
SCRIPT_NAME=$(basename $0)
SCRIPT_NAME="${SCRIPT_NAME%.*}"
TS=$(date '+%Y/%m/%d %H:%M')

LIB_SH_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPO_DIR=$(realpath "${LIB_SH_DIR}/../..")
VENV_DIR="${REPO_DIR}/venv"

##############################################################################
###  Python lib dir
##############################################################################
PYLIB="${REPO_DIR}/lib/py"
export PYTHONPATH=$PYTHONPATH:$PYLIB


##############################################################################
###  Logging
##############################################################################
log_inner() {
  local lineno=$(printf %-4d $1)
  shift
  local lvl=$1
  shift
  local col=$1
  shift
 #  echo -e "${TS}  [${lvl}] (${SCRIPT_NAME}:${BASH_LINENO[0]}:${FUNCNAME[1]}) $*"
  builtin echo -e "${col}[${lvl}]  ${TS} ${SCRIPT_NAME}:${lineno} $*"
}

debug(){ log_inner ${BASH_LINENO[0]} "DEBUG" '\033[1;30m' $* ; }
log()  { log_inner ${BASH_LINENO[0]} "INFO"  '\033[0m' $* ; }
warn() { log_inner ${BASH_LINENO[0]} "WARN"  '\033[0;33m' $* ; }
error(){ log_inner ${BASH_LINENO[0]} "ERROR" '\033[0;31m' $* ; }
fatal(){ log_inner ${BASH_LINENO[0]} 'FATAL' '\033[0;31m' $* ; exit 1 ; }


######################################################
### Enable virtual env
######################################################
if [[ ! -z "${SCHNITT_VENV}" ]] ; then
  debug "Activating virtualenv: $SCHNITT_VENV"
  source "${SCHNITT_VENV}/bin/activate"
fi

#if [[ ! -d "$VENV_DIR" ]] ; then
#  fatal "Cannot find venv dir: '$VENV_DIR' under repo dir"
#else
#  debug "Activating virtualenv: $VENV_DIR"
#  source $VENV_DIR/bin/activate
#fi

