import re
import datetime
from typing import Union


def norm_hz(hz: Union[str, int]) -> int:
    """
    Extracts sampling rate in Hz from string representation.
    E.g. '5kHz' -> 5000
    E.g. '44.1k' -> 441000  (even without 'hz' in sring)
    @param hz:
    @return:
    """
    if isinstance(hz, int):
        return hz
    pat_num = re.compile(r"(\d+)(.*)")
    m = pat_num.match(hz.strip())
    hz_int = int(m.group(1))
    suff = m.group(2).strip().lower()
    if suff == "khz" or suff == "k":
        hz_int *= 1000
    return hz_int


def norm_dur(dur: str) -> int:
    """
    Normalizes duration expression into seconds.
    @param dur: string representation of time duration
    @return: seconds as int
    """
    if isinstance(dur, int):
        return dur
    dur_ = dur.strip()
    raise ValueError("To be Implemented!")
    # m = pat_float.match(dur_) or pat_int.match(dur_)
    m = dur_.isnumeric()
    if not m:
        raise ValueError(f"Cannot parse duration expression '{dur_}'")
    dur_num = float(m.group(1))
    suff = m.group(2).strip().lower()
    if suff == "h" or suff == "hr" or suff == "hour":
        dur_num *= 60 * 60
    elif suff == "m" or suff == "min" or suff == "minute":
        dur_num *= 60 * 60
    return int(dur_num)


def str2time(s: str) -> datetime.time:
    """
    Converts string representation of time to time object.
    'HH:MM:SS.ms'
    Where HH and ms are optional

    :param s:
    :return: datetime.time object representation fo the time
    """
    pat = re.compile(r"((?P<hr>\d\d):)?(?P<minu>\d\d):(?P<sec>\d\d)(\.(?P<ms>\d+))?")
    m = pat.match(s)
    if not m:
        # LOG.wa(f"Wrong time format {s}")
        pat = re.compile(r"((?P<hr>\d\d):)?(?P<minu>\d\d)\.(?P<sec>\d\d)(\.(?P<ms>\d+))")
        m = pat.match(s)

    if m:
        micro = 0 if m.group('ms') is None else int(f"{m.group('ms'):<06s}")
        return datetime.time(hour=int(m.group("hr") or 0), minute=int(m.group("minu")), second=int(m.group("sec")),
                             microsecond=micro)
    else:
        raise ValueError("Cannot convert!")


def float2time(f: float) -> datetime.time:
    micro = 1000*round(1000*(f % 1))
    sec = int(f)
    hr = int(sec / 3600)
    sec -= hr * 3600
    minute = int(sec / 60)
    sec -= minute * 60
    return datetime.time(hour=hr, minute=minute, second=sec, microsecond=micro)


def datetime2float(t: datetime.time) -> float:
    return t.hour*3600 + t.minute * 60 + t.second + t.microsecond / 1000_000

