import logging
from enum import Enum, auto
from typing import Dict, Generator, List

LOG = logging.getLogger(__name__)


class Column(Enum):
    REC_ID = auto()  # recording ID
    CH = auto()  # channel
    START = auto()  # start time
    END = auto()  # end time
    TRS = auto()  # transcription


DEFAULT_COLUMNS = {
        Column.REC_ID: "REC_ID",
        Column.START: "START",
        Column.END: "END",
        Column.CH: "CH",
        Column.TRS: "LABEL"
    }


COLTYPE_TO_ARGNAME = {
        Column.REC_ID: "rec_id",
        Column.START: "start",
        Column.END: "end",
        Column.CH: "ch",
        Column.TRS: "label"
    }


class TrsLine:
    def __init__(self, rec_id: str, ch: int, start: float, end: float, label: str):
        self.rec_id: str = rec_id
        self.ch: int = {"L": 1, "R": 2}.get(ch, ch)
        self.start: float = start
        self.end: float = end
        self.label = label

    def __str__(self):
        label = "" if self.label is None else self.label
        return f"{self.rec_id}\t{self.ch}\t{self.start}\t{self.end}\t{label}"


class TrsParser:
    """
    Transcription label parser.
    """
    def __init__(self, columns: Dict[str, Column] = None):
        if columns is None:
            self.name2col = {v.upper(): k for k, v in DEFAULT_COLUMNS.items()}
        else:
            self.name2col = {k.upper(): v for k, v in columns.items()}

    def parse(self, fpath, col_types: List[Column] = None) -> Generator[TrsLine, None, None]:
        """
        :param fpath:
        :param col_types:  column types
        :return:
        """
        LOG.debug(f"Parsing {fpath}")
        col_ixs: Dict[Column, int]
        line_no = 0
        with open(fpath, encoding="utf-8") as fh:
            if col_types is not None:
                col_ixs = {col: i for i, col in enumerate(col_types)}
            else:  # columns not-specified: try defaults
                line = fh.readline().strip()
                line_no += 1
                hdr_chunks = line.upper().split("\t")
                unk_col_type = [hdr for hdr in hdr_chunks if hdr not in self.name2col]
                if len(unk_col_type) == len(hdr_chunks):  # probably no headers
                    raise ValueError(f"Wrong or missing header in trs file: {fpath}! Add header or specify column types"
                                     f" for the 'parse' function!")
                if len(unk_col_type) > 0:
                    raise ValueError("Unkown column types: {}".format(",".join(unk_col_type)))
                col_ixs = {self.name2col.get(hdr): i for i, hdr in enumerate(hdr_chunks)}

            for line in fh:
                line_no += 1
                line = line.strip()
                chunks = line.split("\t")
                if len(chunks) == 0:
                    continue
                if len(chunks) != len(col_ixs):
                    data = {col: chunks[i] for col, i in col_ixs.items() if i < len(chunks)}
                else:
                    data = {col: chunks[i] for col, i in col_ixs.items()}
                if Column.CH in data:  # cast to int
                    data[Column.CH] = int(data[Column.CH])

                if Column.START in data:  # cast to int
                    data[Column.START] = float(data[Column.START])

                if Column.END in data:  # cast to int
                    data[Column.END] = float(data[Column.END])

                args = {arg: None for arg in COLTYPE_TO_ARGNAME.values()}
                args.update({COLTYPE_TO_ARGNAME.get(col): val for col, val in data.items()})
                yield TrsLine(**args)
