import json
import logging
import os
import socket
from typing import Dict, Union, List


LOG = logging.getLogger(__name__)
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
FNAME_CONF_DATA = "data.json"


def find_per_host_file(fname: str, hostname: str = None) -> Dict[str, str]:
    """
    Finds configuration files with specified name - recursively.
    Returns the parent dir name (assuming it is a stage name) and full path.

    a/b/c/d/conf/<HOST>/<STAGE>/<fname>
    a/b/c/d/conf/<HOST>/<fname>
    a/b/c/conf/<HOST>/<fname>
    a/b/conf/<HOST>/<fname>
    a/conf/<HOST>/<fname>

    @param fname: file to find
    @param hostname:
    @return:
    """
    confs = {}
    hostname_ = hostname if hostname is not None else socket.gethostname()

    def get_staged(by_host: str) -> Dict[str, str]:
        staged_path = {d: os.path.join(by_host, d, fname) for d in os.listdir(by_host)}
        staged_path.update({"": os.path.join(by_host, fname)})
        return {stage: path for stage, path in staged_path.items() if os.path.isfile(path)}

    if "CONF_DIR" in os.environ:
        root = os.environ["CONF_DIR"]
    elif "SCRIPT_DIR" in vars():
        root = SCRIPT_DIR
    else:
        root = os.getcwd()
    while True:
        dpath_conf = os.path.join(root, "conf", "by-host", hostname_)
        if os.path.isdir(dpath_conf):
            confs = get_staged(dpath_conf) | confs
        up = os.path.normpath(os.path.join(root, ".."))
        if up == root:
            break
        else:
            root = up
    return confs


def get_conf_file(fname: str, stage: str = "") -> Union[List, Dict]:
    stage2conf = find_per_host_file(fname)
    if stage not in stage2conf:  # find file w/o hostname
        stage2conf = find_per_host_file(fname, hostname="")
    if stage in stage2conf:
        return stage2conf[stage]
    LOG.error(f"Couldn't find config file '{fname}'")
    return None


def get_data_conf_file(stage: str = "") -> str:
    return get_conf_file(fname=FNAME_CONF_DATA, stage=stage)


def get_conf(fname: str) -> Union[List, Dict]:
    fpath_conf = get_conf_file(fname)
    if fpath_conf:
        with open(fpath_conf, encoding="utf-8") as fh:
            LOG.debug(f"Reading conf file: {fpath_conf}")
            return json.load(fh)
    else:
        return None


def get_data_conf(stage: str = "") -> Union[List, Dict]:
    fpath_conf = get_data_conf_file(stage=stage)
    if fpath_conf is None:
        raise ValueError("Couldn't find trs configuration with name '%s'", FNAME_CONF_DATA)
    with open(fpath_conf, encoding="utf-8") as fh:
        LOG.debug(f"Reading conf file: {fpath_conf}")
        return json.load(fh)


def filter_conf(conf: Dict, **filters) -> List[Dict]:
    """
    Returns object configurations matching filters.
    Usage

    filter_conf(conf, corpus="kids10", hz=8000)

    :param conf:
    :param filters:
    :return:
    """
    hitlist = []
    filter_keys = set(filters.keys())
    for obj in conf:
        is_match = True
        if len(filter_keys - set(obj.keys())) != 0:  # some filter keys are not in the object
            continue
        no_match = False
        for filt in filter_keys:
            if filters[filt] != obj[filt]:
                no_match = True
                break
        if no_match:
            continue
        hitlist.append(obj)
    return hitlist


def get_audio_root(corpus: str, stage: str, **filters) -> str:
    conf_obj_lst = get_data_conf(stage=stage)
    if len(conf_obj_lst) == 0:
        raise ValueError(f"No config file was found for corpus '{corpus}' at stage '{stage}'!")
    filters_ = {k: f"{v}" for k, v in filters.items()} | {"corpus": corpus}
    conf_obj_lst = filter_conf(conf_obj_lst, **filters_)
    if len(conf_obj_lst) == 0:
        raise ValueError(f"No config found for corpus '{corpus}'! Check conf file and filter!")
    if len(conf_obj_lst) > 1:
        raise ValueError("Multiple configs found! Check conf file and filter!")
    return conf_obj_lst[0]["path"]

