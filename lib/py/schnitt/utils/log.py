import logging


def setup_dev_log(lvl: int = logging.DEBUG) -> None:
    """
    Sets up basic config
    :param lvl:
    :return:
    """
    logging.basicConfig(format="%(asctime)s [%(levelname)s] %(module)s.%(funcName)s %(message)s", level=lvl)
