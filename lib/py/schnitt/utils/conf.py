import json
import logging
import os
import socket
from typing import Dict, Union, List

from schnitt.tn.units import norm_hz

LOG = logging.getLogger(__name__)
SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))


def find_per_host_file(fname: str, hostname: str = None) -> str:
    """
    Finds configuration file with specified name recursively:
    a/b/c/d/conf/<HOST>/<fname>
    a/b/c/conf/<HOST>/<fname>
    a/b/conf/<HOST>/<fname>
    a/conf/<HOST>/<fname>

    @param fname: file to find
    @param hostname:
    @return:
    """
    hostname_ = hostname if hostname is not None else socket.gethostname()
    if "SCRIPT_DIR" in vars():
        root = SCRIPT_DIR
    else:
        root = os.getcwd()

    while True:
        fpath_conf = os.path.join(root, "conf", "by-host", hostname_, fname)
        if os.path.isfile(fpath_conf):
            break
        fpath_conf = os.path.join(root, "conf", "by-host", hostname_, fname)
        if os.path.isfile(fpath_conf):
            break
        else:
            fpath_conf = None
        up = os.path.normpath(os.path.join(root, ".."))
        if up == root:
            break
        else:
            root = up
    return fpath_conf


def get_conf_file(fname: str) -> Union[str, None]:
    fpath_conf = find_per_host_file(fname)
    if not fpath_conf:
        fpath_conf = find_per_host_file(fname, hostname="")  # skip hostname
    if fpath_conf:
        return fpath_conf
    else:
        LOG.error(f"Couldn't find config file '{fname}'")
        return None


def get_conf(fname: str) -> Union[List, Dict, None]:
    fpath_conf = get_conf_file(fname)
    if fpath_conf:
        with open(fpath_conf, encoding="utf-8") as fh:
            LOG.debug(f"Reading conf file: {fpath_conf}")
            return json.load(fh)
    else:
        return None


def get_data_conf() -> Union[List, Dict]:
    conf_dict = get_conf("data.json")
    if conf_dict is None:
        raise ValueError("Couldn't find 'data.json'")
    return conf_dict


def get_data_conf_file() -> str:
    return get_conf_file("data.json")


def get_services_conf_file() -> Union[List, Dict]:
    return get_conf("services.json")


def get_services_conf() -> str:
    return get_conf_file("services.json")


def equals(att: str, lhs, rhs, undef_match: bool = True) -> bool:
    if lhs is None and rhs is None:
        return True
    if undef_match and (lhs is None or rhs is None):  # undefined match
        return True
    if att.lower() == "hz":
        return norm_hz(lhs) == norm_hz(rhs)
    return f"{lhs}" == f"{rhs}"


def get_resource_obj(conf: List, **filters) -> List[Dict]:
    """
    Loads resource object from a 'data.json' file
    :param conf:
    :param filters:
    :return:
    """
    found = []
    filter_keys = set(filters.keys())
    for obj in conf:
        is_match = True
        if len(filter_keys - set(obj.keys())) != 0:  # some filter keys are not in the object
            continue
        for filt in filter_keys:
            if not equals(filt, obj[filt], filters[filt]):
                is_match = False
            if not is_match:
                break
        if is_match:
            found.append(obj)
    return found
