import os
from typing import Union, List, Generator


def foreach_file(topdir: str, ext: Union[str, List[str]]) -> Generator[str, None, None]:
    """
    Iterates over files under the specified directory tree.

    :param topdir: root directory to start searching
    :param ext: target extension. Can be a single extension or a list.
    :return:  yields each individual target file
    """
    ext = [ext] if isinstance(ext, str) else ext
    for root, _, fnames in os.walk(topdir):
        for fname in fnames:
            if os.path.splitext(fname)[-1] in ext:
                yield os.path.join(root, fname)

