from enum import Enum
from typing import Union
import re


class AudioEncoding(Enum):
    UNK = 0,
    PCM_SIGNED = 10,
    PCM_UNSIGNED = 11,
    PCM_FLOAT = 12,
    ALAW = 10,
    ULAW = 20,
    GSM = 30,


class AudioFormat:
    """
    Usage:

    AudioFormat.default()
    AudioFormat.get16k(enc=AudioEncoding.PCM_FLOAT)
    """
    @staticmethod
    def default():
        return AudioFormat.get()

    @staticmethod
    def get(hz: float = 8000, bit: int = 16, ch: int = 1, big_endian: bool = False,
            enc: AudioEncoding = AudioEncoding.PCM_SIGNED):
        return AudioFormat(hz, bit, ch, big_endian, enc)

    @staticmethod
    def get16k(bit: int = 16, ch: int = 1, big_endian: bool = False, enc: AudioEncoding = AudioEncoding.PCM_SIGNED):
        return AudioFormat(8000, bit, ch, big_endian, enc)

    def __init__(self, hz: float, bit: int, ch: int, big_endian: bool, enc: AudioEncoding):
        self.hz = hz
        self.bit = bit
        self.ch = ch
        self.big_endian = big_endian
        self.enc = enc
        # handy for processing
        # TODO: check if fields below (a) are really needed, (b) better to be as 'float' for further calculations
        self.bytePerSample = self.bit / 8
        self.bytePerFrame = self.bytePerSample * self.ch

