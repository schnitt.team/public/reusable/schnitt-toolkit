import os
import logging
import wave
from typing import List, Tuple

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
SCRIPT_NAME = os.path.splitext(os.path.basename(__file__))[0]
LOG = logging.getLogger(__name__)


class WaveCutter:
    def __init__(self, path: str):
        self.fh = wave.open(path)
        self.nch = self.fh.getnchannels()
        self.hz = self.fh.getframerate()
        self.depth = self.fh.getsampwidth()  # sample depth in byte
        # self.dtype = {1: np.int8, 2: np.int16, 4: np.int32}.get(self.depth)

    def extract(self, start: float, end: float, sink, header: bool = False, ch: int = -1):
        """
        :param start:
        :param end:
        :param sink:
        :return:
        """
        # set position - in frames
        nframe = int(round((end - start)*self.hz, 3))
        self.fh.setpos(round(start * self.hz))
        frames = self.fh.readframes(nframe)

        if ch < 0:  # write as is
            sink.write(frames)
        elif ch == 0:
            raise ValueError("There is no zero channel. Use 1-based indexing for channels: 1=left, 2=right.")
        elif ch == 1 and self.nch == 1:
            if isinstance(sink, wave.Wave_write):
                sink.writeframesraw(frames)
            else:
                sink.write(frames)
        elif ch <= self.nch:  # write left channel
            if isinstance(sink, wave.Wave_write):
                write_buff = sink._file
                # hacking with WAVE internals to avoid creating another byte buffer
                sink._ensure_header_written(nframe * self.depth)
                sink._datawritten = nframe * self.depth
                sink._nframeswritten = sink._nframeswritten + nframe
            else:
                write_buff = sink
            m = self.depth * self.nch
            shift = (ch - 1) * self.depth
            for i in range(nframe):
                off = i * m + shift
                write_buff.write(frames[off:(off + self.depth)])
        else:
            raise ValueError(f"Cannot get channel {ch}, only {self.nch} channel(s).")


def extract_segments(fpath_wav: str, chunk_info: List[Tuple[str, int, float, float]], outdir: str = None,
                   raw: bool = False, overwrite: bool = True) -> None:
    """
    Utility function to extract intervals from a single file.

    :param fpath_wav:  source audio file; must be in PCM/WAVE format. No need to be mono.
    :param chunk_info: 4-tuples of (REC_ID, CH, START_TIME, END_TIME).
                       CH is 1-based channel index (1 is left). It can be None or -1 if the audio is mono.
    :param outdir:
    :param raw:        flag for writing raw file (.raw) or WAVE file with header (.wav)
    :param overwrite:  flag for overwriting/skipping audio segment
    :return:
    """
    if outdir is not None:
        os.makedirs(outdir, exist_ok=True)
    cutter = WaveCutter(fpath_wav)
    for rec_id, ch, start, end in sorted(chunk_info):
        fpath_chunk = f"{rec_id}-{ch}-{round(start * 1000):08d}-{round(end * 1000):08d}.wav"
        if outdir:
            fpath_chunk = os.path.join(outdir, fpath_chunk)
        if overwrite is False and os.path.isfile(fpath_chunk):
            continue
        if raw:
            with open(fpath_chunk, "wb") as sink:
                cutter.extract(start, end, sink, ch=ch)
        else:
            with wave.open(fpath_chunk, "wb") as sink:
                sink.setframerate(cutter.hz)
                sink.setsampwidth(cutter.depth)
                if ch > 0:
                    sink.setnchannels(1)
                else:
                    sink.setnchannels(cutter.nch)
                cutter.extract(start, end, sink, ch=ch)

