from typing import Union
import re

PAT_NUM = re.compile(r"(\d+)(.*)")


def norm_hz(hz: Union[str, int, float]) -> int:
    """
    Normalizes sampling rate to Hertz (vs. kHz) as float.
    E.g. '5kHz' -> 5000
    E.g. '44.1k' -> 441000  (even without 'hz' in string)
    @param hz:
    @return:
    """
    if isinstance(hz, int):
        return hz
    if isinstance(hz, float):
        return int(hz)
    if isinstance(hz, str):
        m = PAT_NUM.match(hz.strip())
        hz_int = int(m.group(1))
        suff = m.group(2).strip().lower()
        if suff == "khz" or suff == "k":
            hz_int *= 1000
        return hz_int
